﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLinearRegressions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblPoints = New System.Windows.Forms.Label()
        Me.lstPoints = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.btnAddPoint = New System.Windows.Forms.Button()
        Me.txtY = New System.Windows.Forms.TextBox()
        Me.lblY = New System.Windows.Forms.Label()
        Me.txtX = New System.Windows.Forms.TextBox()
        Me.lblX = New System.Windows.Forms.Label()
        Me.grpEquation = New System.Windows.Forms.GroupBox()
        Me.lblEquation = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.grpEquation.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblPoints
        '
        Me.lblPoints.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPoints.Location = New System.Drawing.Point(9, 14)
        Me.lblPoints.Name = "lblPoints"
        Me.lblPoints.Size = New System.Drawing.Size(123, 23)
        Me.lblPoints.TabIndex = 0
        Me.lblPoints.Text = "Points"
        Me.lblPoints.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lstPoints
        '
        Me.lstPoints.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPoints.FormattingEnabled = True
        Me.lstPoints.ItemHeight = 18
        Me.lstPoints.Location = New System.Drawing.Point(12, 40)
        Me.lstPoints.Name = "lstPoints"
        Me.lstPoints.Size = New System.Drawing.Size(120, 184)
        Me.lstPoints.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnReset)
        Me.GroupBox1.Controls.Add(Me.btnCalculate)
        Me.GroupBox1.Controls.Add(Me.btnAddPoint)
        Me.GroupBox1.Controls.Add(Me.txtY)
        Me.GroupBox1.Controls.Add(Me.lblY)
        Me.GroupBox1.Controls.Add(Me.txtX)
        Me.GroupBox1.Controls.Add(Me.lblX)
        Me.GroupBox1.Location = New System.Drawing.Point(148, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(144, 184)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Add New Point"
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(18, 143)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(110, 23)
        Me.btnReset.TabIndex = 6
        Me.btnReset.Text = "Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Enabled = False
        Me.btnCalculate.Location = New System.Drawing.Point(18, 114)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(110, 23)
        Me.btnCalculate.TabIndex = 5
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'btnAddPoint
        '
        Me.btnAddPoint.Location = New System.Drawing.Point(18, 85)
        Me.btnAddPoint.Name = "btnAddPoint"
        Me.btnAddPoint.Size = New System.Drawing.Size(110, 23)
        Me.btnAddPoint.TabIndex = 4
        Me.btnAddPoint.Text = "Add Point"
        Me.btnAddPoint.UseVisualStyleBackColor = True
        '
        'txtY
        '
        Me.txtY.Location = New System.Drawing.Point(38, 48)
        Me.txtY.Name = "txtY"
        Me.txtY.Size = New System.Drawing.Size(90, 20)
        Me.txtY.TabIndex = 3
        '
        'lblY
        '
        Me.lblY.AutoSize = True
        Me.lblY.Location = New System.Drawing.Point(15, 51)
        Me.lblY.Name = "lblY"
        Me.lblY.Size = New System.Drawing.Size(17, 13)
        Me.lblY.TabIndex = 2
        Me.lblY.Text = "Y:"
        '
        'txtX
        '
        Me.txtX.Location = New System.Drawing.Point(38, 22)
        Me.txtX.Name = "txtX"
        Me.txtX.Size = New System.Drawing.Size(90, 20)
        Me.txtX.TabIndex = 1
        '
        'lblX
        '
        Me.lblX.AutoSize = True
        Me.lblX.Location = New System.Drawing.Point(15, 25)
        Me.lblX.Name = "lblX"
        Me.lblX.Size = New System.Drawing.Size(17, 13)
        Me.lblX.TabIndex = 0
        Me.lblX.Text = "X:"
        '
        'grpEquation
        '
        Me.grpEquation.Controls.Add(Me.lblEquation)
        Me.grpEquation.Location = New System.Drawing.Point(13, 230)
        Me.grpEquation.Name = "grpEquation"
        Me.grpEquation.Size = New System.Drawing.Size(279, 45)
        Me.grpEquation.TabIndex = 3
        Me.grpEquation.TabStop = False
        Me.grpEquation.Text = "Final Equation"
        '
        'lblEquation
        '
        Me.lblEquation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEquation.Location = New System.Drawing.Point(6, 19)
        Me.lblEquation.Name = "lblEquation"
        Me.lblEquation.Size = New System.Drawing.Size(267, 23)
        Me.lblEquation.TabIndex = 0
        Me.lblEquation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmLinearRegressions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 283)
        Me.Controls.Add(Me.grpEquation)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lstPoints)
        Me.Controls.Add(Me.lblPoints)
        Me.Name = "frmLinearRegressions"
        Me.Text = "Linear Regressions"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpEquation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblPoints As System.Windows.Forms.Label
    Friend WithEvents lstPoints As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtY As System.Windows.Forms.TextBox
    Friend WithEvents lblY As System.Windows.Forms.Label
    Friend WithEvents txtX As System.Windows.Forms.TextBox
    Friend WithEvents lblX As System.Windows.Forms.Label
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents btnAddPoint As System.Windows.Forms.Button
    Friend WithEvents grpEquation As System.Windows.Forms.GroupBox
    Friend WithEvents lblEquation As System.Windows.Forms.Label

End Class
