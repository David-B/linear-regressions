﻿Public Class frmLinearRegressions
    'Declare Arrays & Variables
    Dim xValues(0) As Double
    Dim yValues(0) As Double
    Dim totalPoints As Integer
    Dim xValuesSum As Double
    Dim yValuesSum As Double

    Private Sub btnAddPoint_Click(sender As Object, e As EventArgs) Handles btnAddPoint.Click
        'Handles the proces for adding points.
        totalPoints += 1
        xValuesSum += txtX.Text
        yValuesSum += txtY.Text
        xValues(totalPoints - 1) = txtX.Text
        yValues(totalPoints - 1) = txtY.Text
        lstPoints.Items.Add("(" & xValues(totalPoints - 1) & "," & yValues(totalPoints - 1) & ")")
        txtX.Clear()
        txtY.Clear()
        btnCalculate.Enabled = True
        ReDim xValues(totalPoints)
        ReDim yValues(totalPoints)
        txtX.Focus()
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        'This is the part that actually takes our points and makes an equation.
        Dim averageX = xValuesSum / totalPoints
        Dim averageY = yValuesSum / totalPoints
        lblEquation.Text = "y = " & averageY / averageX & "x + " & averageY - averageX
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        'This part resets the program to its original settings.
        lstPoints.Items.Clear()
        xValuesSum = 0
        yValuesSum = 0
        totalPoints = 0
        ReDim xValues(0)
        ReDim yValues(0)
        btnAddPoint.Enabled = True
        btnCalculate.Enabled = False
        txtX.Clear()
        txtY.Clear()
        lblEquation.Text = ""
        btnAddPoint.Focus()
    End Sub
End Class
