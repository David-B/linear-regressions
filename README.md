# Linear Regressions #

This program calculates a line of best fit based on coordinates provided by the user. This program was written in Visual Basic as a personal experiment inspired by my Algebra 2 math teacher with no plans for releasing. Because of this, the program does not have an exceptionally beautiful user interface, and will crash if the user does something wrong (ie. providing invalid input when trying to add a point results in a crash).

# Where can I download a copy of the program? #

The Linear Regressions program is available on the [repository download page](https://bitbucket.org/David-B/linear-regressions/downloads).

# How does this program calculate the line of best fit? #

To calculate the line of best fit, this program finds a slope for the line by dividing the average (arithmetic mean) of the Y values of the points entered by the user by the average of the X values of the points entered by the user. The program then calculates the Y-intercept for the line by subtracting the average of the X values from the average of the Y values. This information is then used to present the line of best fit to the user in point-slope form.


Here is the code excerpt that performs the tasks explained above. This excerpt does not show the portion of the arithmetic mean calculation in which the X and Y values are added together. We can assume that these values are stored in the variables `xValuesSum` and `yValuesSum` respectively.

```
#!vbnet

Dim averageX = xValuesSum / totalPoints
Dim averageY = yValuesSum / totalPoints
lblEquation.Text = "y = " & averageY / averageX & "x + " & averageY - averageX
```

## Redistribution License (MIT) ##

The MIT License (MIT)

Copyright (c) 2015 David Berdik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.